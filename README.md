MfGames.Locking CIL
===================

This a small collection of classes that provide some simplification while working with locking in C# applications.

```
//using MfGames.Locking;

ReaderWriterLockSlim locker;

using (new ReadLock(locker))
{
    // Do something.
}
```

## Building

The repository is set up to use [asdf]() for the environment. If you don't use this tool, then ensure the following are installed and available:

- .NET 5.0.100 or later
- Yarn 1.22.10 or later
- NodeJS 15.0.1 or later
