## [1.1.6](https://gitlab.com/mfgames-cil/mfgames-locking-cil/compare/v1.1.5...v1.1.6) (2021-09-11)


### Bug Fixes

* **build:** fixing issues with deployment ([70d6f57](https://gitlab.com/mfgames-cil/mfgames-locking-cil/commit/70d6f57d7c7c2f1e71d752029065f240a523ea39))

## [1.1.5](https://gitlab.com/mfgames-cil/mfgames-locking-cil/compare/v1.1.4...v1.1.5) (2021-09-11)


### Bug Fixes

* **nuget:** fixing packaging and versioning ([1c375c4](https://gitlab.com/mfgames-cil/mfgames-locking-cil/commit/1c375c45cc22eeb27ebec60eb45e934cd1bf28b3))
* **nuget:** fixing packaging and versioning ([0ce47dd](https://gitlab.com/mfgames-cil/mfgames-locking-cil/commit/0ce47dd8d186bb312ad6a3da64139f20c07674a1))
* **nuget:** fixing packaging and versioning ([7bc5aac](https://gitlab.com/mfgames-cil/mfgames-locking-cil/commit/7bc5aacc4b87e151d85640501f27a6bbacc9a743))

## [1.1.4](https://gitlab.com/mfgames-cil/mfgames-locking-cil/compare/v1.1.3...v1.1.4) (2021-09-04)


### Bug Fixes

* **semantic-release-nuget:** fixing typo ([4ce4fe2](https://gitlab.com/mfgames-cil/mfgames-locking-cil/commit/4ce4fe26cb3d8e90a7ef710a82626af8e7caa147))

## [1.1.3](https://gitlab.com/mfgames-cil/mfgames-locking-cil/compare/v1.1.2...v1.1.3) (2021-09-04)


### Bug Fixes

* working on pushing ([5c66619](https://gitlab.com/mfgames-cil/mfgames-locking-cil/commit/5c66619e12587ada01c5eafd5a4aa5d8f6ef3c5d))

## [1.1.2](https://gitlab.com/mfgames-cil/mfgames-locking-cil/compare/v1.1.1...v1.1.2) (2021-09-04)


### Bug Fixes

* updating package-lock.json ([d000882](https://gitlab.com/mfgames-cil/mfgames-locking-cil/commit/d000882f96895ffa42c000f0169048354db78bc1))
* working on build patterns ([7594b73](https://gitlab.com/mfgames-cil/mfgames-locking-cil/commit/7594b73846b80127ee6ba7ab1d3d0f6d64c81cc5))
* working on dependencies ([ba538c0](https://gitlab.com/mfgames-cil/mfgames-locking-cil/commit/ba538c0d86005618c84f3fc6bbf062d60e499a1b))

## [1.1.1](https://gitlab.com/mfgames-cil/mfgames-locking-cil/compare/v1.1.0...v1.1.1) (2021-01-22)


### Bug Fixes

* added changelog generation ([d8fa302](https://gitlab.com/mfgames-cil/mfgames-locking-cil/commit/d8fa3029a9c2d0c9523584a78e16d4612fd114f8))
