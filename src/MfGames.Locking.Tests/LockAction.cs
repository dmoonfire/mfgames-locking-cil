namespace MfGames.Locking.Tests
{
    /// <summary>
    /// The various states for testing lock logic.
    /// </summary>
    internal enum LockAction
    {
        BeforeLock,

        InLock,

        AfterLock,
    }
}
