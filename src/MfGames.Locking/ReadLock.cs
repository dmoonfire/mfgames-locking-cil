﻿using System;
using System.Threading;

namespace MfGames.Locking
{
    /// <inheritdoc />
    /// <summary>
    /// Defines a ReaderWriterLockSlim read-only lock.
    /// </summary>
    public class ReadLock : IDisposable
    {
        private readonly ReaderWriterLockSlim readerWriterLockSlim;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReadLock" /> class.
        /// </summary>
        /// <param name="readerWriterLockSlim">The reader writer lock slim.</param>
        public ReadLock(ReaderWriterLockSlim readerWriterLockSlim)
        {
            this.readerWriterLockSlim = readerWriterLockSlim;
            readerWriterLockSlim.EnterReadLock();
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.readerWriterLockSlim.ExitReadLock();
        }
    }
}
