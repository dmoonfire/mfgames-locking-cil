﻿using System;
using System.Threading;

namespace MfGames.Locking
{
    /// <summary>
    /// Defines a ReaderWriterLockSlim write lock.
    /// </summary>
    public class NestableWriteLock : IDisposable
    {
        private readonly bool lockAcquired;

        private readonly ReaderWriterLockSlim readerWriterLockSlim;

        /// <summary>
        /// Initializes a new instance of the <see cref="NestableWriteLock" /> class.
        /// </summary>
        /// <param name="readerWriterLockSlim">The reader writer lock slim.</param>
        public NestableWriteLock(ReaderWriterLockSlim readerWriterLockSlim)
        {
            // Keep track of the lock since we'll need it to release the lock.
            this.readerWriterLockSlim = readerWriterLockSlim;

            // If we already have a read or write lock, we don't do anything.
            if (readerWriterLockSlim.IsWriteLockHeld)
            {
                this.lockAcquired = false;
            }
            else
            {
                readerWriterLockSlim.EnterWriteLock();
                this.lockAcquired = true;
            }
        }

        /// <inheritdoc />
        public void Dispose()
        {
            if (this.lockAcquired)
            {
                this.readerWriterLockSlim.ExitWriteLock();
            }
        }
    }
}
