using System;
using System.Threading;

namespace MfGames.Locking
{
    /// <summary>
    /// Defines a ReaderWriterLockSlim read-only lock.
    /// </summary>
    public class UpgradableLock : IDisposable
    {
        private readonly ReaderWriterLockSlim readerWriterLockSlim;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpgradableLock" /> class.
        /// </summary>
        /// <param name="readerWriterLockSlim">The reader writer lock slim.</param>
        public UpgradableLock(ReaderWriterLockSlim readerWriterLockSlim)
        {
            this.readerWriterLockSlim = readerWriterLockSlim;
            readerWriterLockSlim.EnterUpgradeableReadLock();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        /// resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.readerWriterLockSlim.ExitUpgradeableReadLock();
        }
    }
}
